#! $HOME/SOFTWARE/anaconda/envs/VISSIM/bin/ python


"""

:History:

22 Oct 2015: created

:author: Ruyman Azzollini (MSSL)
:contact: r.azzollini_at_ucl.ac.uk

"""

from numpy.distutils.core import setup,Extension
from numpy.distutils.misc_util import Configuration

import sys
import os

from pdb import set_trace as stop


def configuration():
    
    config = Configuration()
    
    config.add_subpackage('vissim')
    config.add_subpackage('vissim/analysis')
    config.add_subpackage('vissim/charact')
    config.add_subpackage('vissim/FMcalib')
    config.add_subpackage('vissim/CTI')
    config.add_subpackage('vissim/datamodel')
    #config.add_subpackage('vissim/doc')
    config.add_subpackage('vissim/ETC')
    config.add_subpackage('vissim/fitting')
    config.add_subpackage('vissim/fortran')
    config.add_subpackage('vissim/objects')
    config.add_subpackage('vissim/plotting')
    config.add_subpackage('vissim/postproc')
    config.add_subpackage('vissim/reduction')
    config.add_subpackage('vissim/sandbox')
    config.add_subpackage('vissim/simulator')
    config.add_subpackage('vissim/sources')
    config.add_subpackage('vissim/support')
    
    config.add_data_dir(['vissim/data','vissim/data'])
    config.add_data_dir(['vissim/doc','vissim/doc'])
    
    
    config.add_extension(name='cdm03bidir',sources=['vissim/fortran/cdm03bidir.f90'])
    
    config.add_scripts(['vissim/scripts/vissim',
           'vissim/scripts/vs_tileFPA',
           'vissim/scripts/vs_tileCCD',
           'vissim/scripts/HKmonitor.py',
           'vissim/scripts/QLA.py',
           'vissim/scripts/quickds9.py'])
   
    
    return config


def setup_package():
        
    metadata = dict(
     name="vissim",
     version="1.5.5",
     description="Euclid VIS Simulator",
     author="Sami Matias Niemi, Ruyman Azzollini",
     author_email="ruyman.azzollini@gmail.com",
     url="https://bitbucket.org/RuymanElDeAna/euclid_vis_sim",
     configuration=configuration)
    
    setup(**metadata)
    
    return
    

if __name__ == '__main__': setup_package()

