#! $HOME/SOFTWARE/anaconda/envs/VISSIM/bin/ python


"""

:History:

22 Oct 2015: created

@author: Ruyman Azzollini (MSSL)
"""

from setuptools import setup, Extension
#from numpy.distutils.core import setup,Extension

import sys

from pdb import set_trace as stop

cdm03bidir = Extension(name='cdm03bidir',sources=['vissim/fortran/cdm03bidir.f90'])


setup(
    name="vissim",
    version="1.4.1",
    description="Euclid VIS Simulator",
    author="Sami Matias Niemi, Ruyman Azzollini",
    author_email="ruyman.azzollini@gmail.com",
    url="https://bitbucket.org/RuymanElDeAna/euclid_vis_sim",
    long_description=__doc__,
    packages=['vissim',
       'vissim.analysis',
       'vissim.CTI',
       'vissim.ETC',
       'vissim.fitting',
       'vissim.objects',
       'vissim.plotting',
       'vissim.postproc',
       'vissim.reduction',
       'vissim.sandbox',
       'vissim.simulator',
       'vissim.sources',
       'vissim.support'],
    package_dir={'vissim':'vissim/',
    'vissim.analysis':'vissim/analysis/',
    'vissim.CTI':'vissim/CTI/',
    'vissim.ETC':'vissim/ETC/',
    'vissim.fitting':'vissim/fitting/',
    'vissim.objects':'vissim/objects/',
    'vissim.plotting':'vissim/plotting/',
    'vissim.postproc':'vissim/postproc/',
    'vissim.reduction':'vissim/reduction/',
    'vissim.sandbox':'vissim/sandbox/',
    'vissim.simulator':'vissim/simulator/',
    'vissim.sources':'vissim/sources/',
    'vissim.support':'vissim/support/'},
    package_data={'vissim':['vissim/data/*']},
    ext_modules=[cdm03bidir],
    scripts=['vissim/scripts/vissim'],
    include_package_data=True,
    zip_safe=False,
)
