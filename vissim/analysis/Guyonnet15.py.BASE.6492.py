#! /home/raf/SOFTWARE/anaconda2/envs/VISSIM/bin/python
## /unsafe/raf/SOFTWARE/anaconda/envs/VISSIM/bin/python
## /home/raf/SOFTWARE/anaconda2/envs/VISSIM/bin/python
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 22 11:38:24 2016


Library with functions to replicate results of Guyonnet+15.
"Evidence for self-interaction of charge distribution in CCDs"
Guyonnet, Astier, Antilogus, Regnault and Doherty 2015


Notes:

    - I rename the "X" coordinate (boundary id) with "b", to avoid confusion
      with cartesian "x". 
    - In paper, X belongsto [(0,1),(1,0),(0,-1),(-1,0)]. Here
       b belongsto [0,90,180,270]. They are linked to matrix index 
       ib belongsto [0,1,2,3]
       
    

@author: raf
"""

# IMPORT STUFF
from pdb import set_trace as stop

import numpy as np
from scipy.special import expi as Ei
from scipy import optimize as optimize
import copy
from scipy.signal import convolve2d

from astropy.io import fits as fts
from matplotlib import pyplot as plt
# END IMPORT

def frdist(i,j,ib):
    """ """
    x = i*1. + np.cos((1.-ib)*90*np.pi/180.)*0.5
    y = j*1. + np.sin((1.-ib)*90*np.pi/180.)*0.5
    
    return (x*x+y*y)**0.5

def ftheta_bound(i,j,ib):
    """theta_i,j^X is the angle between the source-boundary vector and the
    normal to the boundary."""
    
    # source-boundary vector    
    sb_v = np.array([np.cos((1.-ib)*90*np.pi/180.)*0.5-i*1.,
                     np.sin((1.-ib)*90.*np.pi/180.)*0.5-j*1.])
    
    # normal vector # unit vector pointing outwards
    n_v = np.array([np.cos((1.-ib)*90*np.pi/180.),
                    np.sin((1.-ib)*90.*np.pi/180.)])
    
    arg = np.dot(sb_v,n_v)
    modulus = np.sqrt(np.dot(sb_v,sb_v)) * 1.
    
    #if i==0 and j>0 and ib==0: stop()
    
    return np.arccos(arg/modulus)
    

def fpred_aijb(p,i,j,ib):
    """Eq. 18"""
    
    p0,p1 = p
    
    rdist = frdist(i,j,ib)
    
    fr = p0 * (-Ei(p1*rdist))
    
    theta_ijb = ftheta_bound(i,j,ib)
    
    aijb = fr * np.cos(theta_ijb)
    
    #if np.abs(aijb) > 3.: stop()

    return aijb


def fun_p(x,*p):
    # k = [i,j]
    
    ii = x[0,:]
    jj = x[1,:]
    
    model = np.zeros(len(ii),dtype='float32')
    
    for ix in range(len(ii)):
        i = ii[ix]
        j = jj[ix]
        for ib in range(4):    
            model[ix] += fpred_aijb(p,i,j,ib)
    #print model
    return model

def fun_g(r,*p):
    
    model = np.exp(-r**2./(2.*p[0]**2.))
    return model

def solve_for_psmooth(covij,var,mu,doplot=False):
    """Solving (p0,p1) parameters in Eq. 18 using covariance matrix."""
    
    ni,nj = covij.shape
    
    assert ni == nj
    N = ni    
    
    i = np.arange(N)
    j = np.arange(N)
    
    ii,jj = np.meshgrid(i,j,indexing='xy')
    
    ixsel = np.where((ii.flatten() > 1) & (jj.flatten()>1))
    #ixsel = np.where(ii.flatten()) # TESTS
    nsel = len(ixsel[0])
    
    xdata = np.zeros((2,nsel),dtype='float32')
    xdata[0,:] = ii.flatten()[ixsel]
    xdata[1,:] = jj.flatten()[ixsel]
    
    r = (xdata[0,:]**2.+xdata[1,:]**2.)**0.5 * 12. # um
    ydata = (covij/(var*mu)).flatten()[ixsel]
    
    order = r.argsort()
    r = r[order]
    ydata = ydata[order].copy()
    xdata[0,:] = xdata[0,order].copy()
    xdata[1,:] = xdata[1,order].copy()
    
    p0 = (1.E-4,1.E-5)

    #xplot = ((xdata[0,:]**2.+xdata[1,:]**2.)**0.5)*12. # um
    # Fit
    
    popt,pcov = optimize.curve_fit(fun_p,xdata,ydata,p0=p0,sigma=None,absolute_sigma=False)
    
    #p0 = (1.3,) # TESTS 
    
    #popt,pcov = optimize.curve_fit(fun_g,r,ydata,p0=p0,sigma=ydata*1.E-3,absolute_sigma=False) # TESTS
    
    epopt = np.sqrt(np.diagonal(pcov))
    
    if doplot:

        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(r,ydata,'ko-',label='data')
        ax.plot(r,fun_p(xdata,*p0),'ro:',label='initial guess')
        ax.plot(r,fun_p(xdata,*popt),'bo:',label='solution')
        ax.set_xlabel('Distance (um)')
        ax.set_ylabel('covij/(var*mu)')
        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles,labels,loc='upper right')
        plt.tight_layout()
        plt.show()
    
    return popt,epopt

def _build_aijx(X,indepBounds,psmooth):
    """ 
    
    indexes of aijx
    
    i : 0, n-1
    j : 0, n-1
    x : 0, 3 (N,E,S,W) : (0, 90, 180, 270)
    
    
    """
    N = int((len(X)+1)**0.5)
    aijx = np.zeros((N,N,4),dtype='float32') + np.nan
    
    # First we asign the independent coefficients
    
    for zx, indepBound in enumerate(indepBounds): 
        i,j,x = indepBound
        aijx[i,j,x] = X[zx]
    
    # "Uncompression" of the coefficients
    # It must be done in reverse order to the "compression"
    
    
    # First elements not on First Row or First Column
    
    # North of lower is like South of upper, if not at top floor
    
    for ip in range(N-1,1-1,-1):
        for jp in range(N-1,1-1,-1):
              
            
            if jp < N-1:
                # North of lower is like South of upper, if not at top
                aijx[ip,jp,0] = aijx[ip,jp+1,2]
            else:
                # if at top, use prediction
                aijx[ip,jp,0] = fpred_aijb(psmooth[0],ip,jp,0)

    # West is like South times factor
    
    for ip in range(N-1,1-1,-1):
        for jp in range(N-1,1-1,-1):
            
            ratioWS = fpred_aijb(psmooth[0],ip,jp,3.)/fpred_aijb(psmooth[0],ip,jp,2.)            
            aijx[ip,jp,3] = aijx[ip,jp,2] * ratioWS
    
    
    # East is like west of right
    
    for ip in range(N-1,1-1,-1):
        for jp in range(N-1,1-1,-1):
            
            if ip<N-1:
                aijx[ip,jp,1] = aijx[ip+1,jp,3]
            else:
                aijx[ip,jp,1] = fpred_aijb(psmooth[0],ip,jp,1.) # prediction
    
    # First Row: jp==0
    
    jp=0
    for ip in range(N-1,1-1,-1):
        
        if ip < N-1:
            # East is like West of Right
            aijx[ip,jp,1] = aijx[ip+1,jp,3]
        elif ip == N-1:
            aijx[ip,jp,1] = fpred_aijb(psmooth[0],ip,jp,1.) # predict
        
        # North is like south of upper
        aijx[ip,jp,0] = aijx[ip,jp+1,2]
        
        # South is like North on the horizontal
        aijx[ip,jp,2] = aijx[ip,jp,0]
        
    # First Column: ip == 0
    
    ip = 0
    
    for jp in range(N-1,-1,-1):
        
        
        if (jp>=0) and (jp < N-1):
            # North is like South of Upper if not at top
            aijx[ip,jp,0] = aijx[ip,jp+1,2]
        elif jp == N-1:
            aijx[ip,jp,0] = fpred_aijb(psmooth[0],ip,jp,0.)
        
        
        if jp ==0:
            # South is like North on the (0,0) pixel
            aijx[ip,jp,2] = aijx[ip,jp,0]
        
        #East is like West of Right
        aijx[ip,jp,1] = aijx[ip+1,jp,3]
        
        # West is like East on the vertical
        aijx[ip,jp,3] = aijx[ip,jp,1]
        
    
    
    return aijx
    


def solve_for_A_linalg(covij,var=1.,mu=1.,doplot=False,psmooth=None):
    """Function to retrieve the A matrix of pixel boundaries
    deformations given a matrix of pixel covariances, variance, and mu.
    
    if var==1 and mu==1, it is understood that covij is
    the correlation matrix.
    
    Now doing Linear Algebra (as it must be done).
    
    See section 6.1
    
    """
    assert isinstance(covij,np.ndarray)
    assert covij.shape[0] == covij.shape[1]
    
    N = int((covij.size)**0.5) # side of cov matrix
    
    if psmooth is None:
        psmooth = solve_for_psmooth(covij,var,mu,doplot=doplot) # smoothing parameters
    
    Rij = covij / (var*mu)
    
    B = np.zeros(N**2-1,dtype='float32') # Independent terms
    
    for i in range(N):
        for j in range(N):
            if i == 0 and j==0: continue

            ix = i*N+j - 1
            
            B[ix] = Rij[i,j]
    
    Afull = np.zeros((4*N**2,N**2-1),dtype='float32') # N-bounds x N-corr
        
    # Boundaries Names List
    
    
    boundsList = []
    for i in range(N):
        for j in range(N):
            for x in range(4):
                boundsList.append((i,j,x))
    
    # Full Coefficients Matrix, Without Reduction
    
    for i in range(N):
        for j in range(N):
            
            if i == 0 and j==0: continue
            
            jyy = i * N + j - 1 # correlation index
            
            for ip in range(N):
                for jp in range(N):
                    
                    for x in range(4):
                        
                        ixx = (ip * N + jp)*4 + x
                        
                        if (i==ip) and (j==jp): Afull[ixx,jyy] = 1.
    
    
    print 'sum(A)=%.1f' % Afull.sum()

    #  REDUCING THE COEFFICIENTS MATRIX
    
    for i in range(N):
        for j in range(N):
            
            if i ==0 and j==0: continue
            
            jyy = i * N + j -1 # correlation index
            
            print '1st column'
            # 1st Column: ip == 0
            
            ip = 0
            for jp in range(N):
                
                ixx = (ip * N + jp)*4
                
                ixxN = ixx + 0 #
                ixxE = ixx + 1 #
                ixxS = ixx + 2 #
                ixxW = ixx + 3 #
                
                ixN=(ip * N + jp+1)*4 #
                ixE=((ip+1) * N + jp)*4 #
                #print ip, jp
                
                # East is like West on the vertical

                Afull[ixxE,jyy] += Afull[ixxW,jyy]
                Afull[ixxW,jyy] = 0.
                
                # West of Right is like East 
                Afull[ixE+3,jyy] += Afull[ixxE,jyy]
                Afull[ixxE,jyy] = 0.
                
                if jp ==0: #(0,0)
                    # North is like South on the (0,0) pixel
                    Afull[ixxN,jyy] += Afull[ixxS,jyy]
                    Afull[ixxS,jyy] = 0. 
                
                if (jp>=0) and (jp < N-1):
                    # South of Upper is like North if not at top
                    Afull[ixN+2,jyy] += Afull[ixxN,jyy]
                    Afull[ixxN,jyy] = 0.
                elif jp == N-1:
                    # if at top, add constant to B
                    Afull[ixxN,jyy] = 0.
                    B[jyy] -= fpred_aijb(psmooth[0],ip,jp,0)                                 
                                   # f(rij)*np.cos(theta_ijx) = 
                                   # p0 Ei(p1 r) * np.cos(theta_ijx)                        
            
            print 'First Row'
            
            # First Row: jp==0 (excluding (0,0))
            
            jp = 0
            for ip in range(1,N):
                
                ixx = (ip * N + jp)*4
                
                ixxN = ixx + 0 #
                ixxE = ixx + 1 #             
                ixxS = ixx + 2 #
                
                ixN=(ip * N + jp+1)*4 #
                ixE=((ip+1) * N + jp)*4 #
                
                
                # North is like South on the horizontal
                Afull[ixxN,jyy] += Afull[ixxS,jyy]
                Afull[ixxS,jyy] = 0.
                    
                # South of Upper is like North if not at top
                Afull[ixN+2,jyy] += Afull[ixxN,jyy]
                Afull[ixxN,jyy] = 0.
                
                
                if (ip < N-1):
                    # West of Right is like East
                    Afull[ixE+3,jyy] += Afull[ixxE,jyy]
                    Afull[ixxE,jyy] = 0.
                elif ip == N-1:
                    Afull[ixxE,jyy] = 0.
                    if i==ip and j==jp:
                        B[jyy] -= fpred_aijb(psmooth[0],ip,jp,1.)
                    #print 'here'
                    #stop()
            
            
            print 'The Rest'
            
            # The rest
            
            # West of Right is like East

            for ip in range(1,N):
                for jp in range(1,N):
                    
                    ixx = (ip * N + jp)*4 #
                    ixxE = ixx + 1 #
                    ixE= ((ip+1) * N + jp)*4 #
            
                    if ip < N-1:
                        # West of Right is like East
                        Afull[ixE+3,jyy] += Afull[ixxE,jyy]
                        Afull[ixxE,jyy] = 0.
                    else:
                        Afull[ixxE,jyy] = 0.
                        if i==ip and j==jp:
                            B[jyy] -= fpred_aijb(psmooth[0],ip,jp,1.) 

            # South is like West times factor
            
            for ip in range(1,N):
                for jp in range(1,N):
                    
                    ixx = (ip * N + jp)*4
                    ixxS = ixx + 2 #
                    ixxW = ixx + 3 #
                        
                    # South is like West times factor 
                    ratioSW = fpred_aijb(psmooth[0],ip,jp,2.)/fpred_aijb(psmooth[0],ip,jp,3.)
                    #print ratioSW, (Afull[ixxW,jyy]) * ratioSW 
                    #print ratioSW
                    Afull[ixxS,jyy] += (Afull[ixxW,jyy]) * ratioSW 
                    Afull[ixxW,jyy] = 0 
                    
            # South of upper is like North if not at top floor

            for ip in range(1,N):
                for jp in range(1,N):
                    
                    ixx = (ip * N + jp)*4
                    ixxN = ixx + 0 #
                    
                    ixN=(ip * N + jp+1)*4 #
                
                            
                    if jp < N-1:
                        # South of Upper is like North if not at top
                        Afull[ixN+2,jyy] += Afull[ixxN,jyy]
                        Afull[ixxN,jyy] = 0.
                    else:
                        # if at top, add constant to B
                        Afull[ixxN,jyy] = 0.
                        if i == ip and j==jp: 
                            B[jyy] -= fpred_aijb(psmooth[0],ip,jp,0.) 
       
    
    
    # Prunning the A matrix
    
    A = np.zeros((N**2-1,N**2-1),dtype='float32')
    indepBounds = []
    
    ixp = 0
    Nraw = 4*N**2
    for ix in range(Nraw):
        if Afull[ix,:].sum() != 0:
            A[ixp,:] = Afull[ix,:].copy()
            indepBounds.append(boundsList[ix])
            ixp += 1
    
    Nindep = len(indepBounds)
    
    print '\n Independent Coeffs. = %i / %i' % (Nindep,Nraw)
    print '\n A.shape = ',A.shape
    

    #i, j = 1,2
    #ixx = (i * N + j)*4
    #jyy =  i * N + j - 1
    #print np.where(Afull[:,jyy] != 0)

    X = np.linalg.solve(A,B)
    
    aijx = _build_aijx(X,indepBounds,psmooth)
    
    # check clossure: sum_ij aijx = 0 for every x
    
    #closure = 0.
    #for x in range(4):
    #    closure += np.sum(np.sum(aijx[:,:,x],axis=0),axis=0)
    
    #print 'Closure = %.2e (==? 0.)' % closure
    
    # verify matrix
    
    res = Rij - aijx.sum(axis=2)
    print 'mean res = %.1e' % res.mean()
    print 'max res = %.1e' % res.max()
    print 'min res = %.1e' % res.min()
    
    
    return aijx


def _build_aijx_synth(psmooth,N):
    """ """
    
    aijx = np.zeros((N,N,4),dtype='float32') + np.nan
    
    for ip in range(N):
        for jp in range(N):
            for x in range(4):
                aijx[ip,jp,x] = fpred_aijb(psmooth,ip,jp,x)
    
    return aijx

#def get_deltaQ_hardandwrong(img,aijx):
#    """ """
#    
#    N,Np,shouldbe4 = aijx.shape
#    assert N == Np
#    
#    dimg = np.zeros_like(img)
#    
#    NX,NY = img.shape
#    
#    # N E S W
#    
#    di = [0,1,0,-1]
#    dj = [1,0,-1,0]
#    
#    # "polarity" of correction
#    #    Quadrants: 1st, 4th, 2nd,, 3rd 
#    p = {0:[1.,1.,-1.,-1.], \
#             1:[1.,-1.,1.,-1.]} # "x" odd (E,W)
#    
#    for i in range(NX):
#        for j in range(NY):
#            
#            for ip in range(N):
#                for jp in range(N):
#                    
#                    if i+ip < NX and j+jp < NY:
#                        for x in range(4):
#                            
#                            # 1st Quadrant
#                            try:
#                                dimg[i,j] += 1./4. * p[x % 2][0] * aijx[ip,jp,x] * \
#                                   img[i+ip,j+jp] * (img[i,j]+img[i+di[x],j+dj[x]])
#                            except IndexError:
#                                dimg[i,j] += 0.
#                            
#                            
#                            # 2nd Quadrant
#                            
#                            try:
#                                dimg[i,j] += 1./4. * p[x % 2][2] * aijx[ip,jp,x] * \
#                                    img[i+ip,j-jp] * (img[i,j]+img[i+di[x],j+dj[x]])
#                            except IndexError:
#                                dimg[i,j] += 0.
#                            
#                            
#                            # 3rd Quad.
#                            
#                            try:
#                                dimg[i,j] += 1./4. * p[x % 2][3] * aijx[ip,jp,x] * \
#                                    img[i-ip,j-jp] * (img[i,j]+img[i+di[x],j+dj[x]])
#                            except IndexError:
#                                dimg[i,j] += 0.
#                            
#                            
#                            # 4th Quadrant
#                            
#                            try:
#                                dimg[i,j] += 1./4. * p[x % 2][1] * aijx[ip,jp,x] * \
#                                    img[i-ip,j+jp] * (img[i,j]+img[i+di[x],j+dj[x]])
#                            except IndexError:
#                                dimg[i,j] += 0.
#                                          
#    return dimg
    
def get_deltaQ(img,aijx):
    """ """
    from astropy.io import fits as fts
    
    N,Np,shouldbe4 = aijx.shape
    assert N == Np
    assert shouldbe4 == 4
    
    kernel = np.zeros((2*N-1,2*N-1,4),dtype='float32') + np.nan
    
    for ixside in range(4):
        
        kernel[N-1:,N-1:,ixside] = aijx[:,:,ixside]# first Quadrant
        kernel[N-1:,0:N-1,ixside] = aijx[:,::-1,ixside][:,0:-1] #2nd Quadrant
        kernel[0:N-1,0:N,ixside] = aijx[::-1,::-1,ixside][0:-1,:] # 3rd Quadrant
        kernel[0:N-1,N:,ixside] = aijx[::-1,:,ixside][0:-1,1:] # 4th Quadrant
    
    
    fts.writeto('kernel_North.fits',kernel[:,:,0].transpose(),clobber=True)
    fts.writeto('kernel_East.fits',kernel[:,:,1].transpose(),clobber=True)
    fts.writeto('kernel_South.fits',kernel[:,:,2].transpose(),clobber=True)
    fts.writeto('kernel_West.fits',kernel[:,:,3].transpose(),clobber=True)
    
    dimg = np.zeros_like(img,dtype='float32')
    
    shift = [[-1,0],[-1,1],[1,0],[1,1]]
    #shift = [[-1,1],[-1,0],[1,1],[1,0]]
    #shift = [[-1,0],[0,-1],[1,0],[0,1]]
    
    for ixside in range(4):
        sidekernel = kernel[:,:,ixside].copy()
        dimg +=  1/4. * (img + np.roll(img,shift[ixside][0],shift[ixside][1])) * \
             convolve2d(img,sidekernel,mode='same') # Eq. 11
    
    return dimg
    
    

def degrade_estatic(img,aijx):
    """Degrades an image according to matrix of pixel-boundaries deformations.
    Follows on Eq. 11 of G15.
    
    """    
    dimg = get_deltaQ(img,aijx)    
    return img + dimg
    
def correct_estatic(img,aijx):
    """Corrects an image from pixel-boundaries deformation due to
    electrostatic forces.
    """
    dimg = get_deltaQ(img,aijx)
    return img - dimg
    


def plot_maps_ftheta(f,ii,jj,suptitle=''):
    
    fig = plt.figure()
    
    i = ii.flatten().copy()
    j = jj.flatten().copy()
    
    f_N = np.zeros(len(i))
    f_E = np.zeros(len(i))
    f_S = np.zeros(len(i))
    f_W = np.zeros(len(i))
    
    for ix in range(len(i)):
        f_N[ix] = f(i[ix],j[ix],0)
        f_E[ix] = f(i[ix],j[ix],1)
        f_S[ix] = f(i[ix],j[ix],2)
        f_W[ix] = f(i[ix],j[ix],3)
        
    f_N = f_N.reshape(ii.shape)
    f_E = f_E.reshape(ii.shape)
    f_S = f_S.reshape(ii.shape)
    f_W = f_W.reshape(ii.shape)
    
    mini = i.min()
    maxi = i.max()
    minj = j.min()
    maxj = j.max()

    
    ax1 = fig.add_subplot(221)
    ax1.imshow(np.cos(f_N),cmap=plt.cm.Blues,interpolation='none',extent=[mini,maxi,minj,maxj],origin='lower left')
    ax1.set_title('N')
    
    ax2 = fig.add_subplot(222)
    ax2.imshow(np.cos(f_E),cmap=plt.cm.Greens,interpolation='none',extent=[mini,maxi,minj,maxj],origin='lower left')
    ax2.set_title('E')
    
    ax3 = fig.add_subplot(223)
    ax3.imshow(np.cos(f_S),cmap=plt.cm.Reds,interpolation='none',extent=[mini,maxi,minj,maxj],origin='lower left')
    ax3.set_title('S')
    
    ax4 = fig.add_subplot(224)
    ax4.imshow(np.cos(f_W),cmap=plt.cm.Oranges,interpolation='none',extent=[mini,maxi,minj,maxj],origin='lower left')
    ax4.set_title('W')
    
    plt.suptitle(suptitle)
    
    plt.show()
    
    plt.close()
    
    #stop()
    

def plot_map(z,ii,jj,title=''):
    
    fig = plt.figure()
    
    i = ii.flatten().copy()
    j = jj.flatten().copy()
    
    mini = i.min()
    maxi = i.max()
    minj = j.min()
    maxj = j.max()

    
    ax1 = fig.add_subplot(111)
    cax1= ax1.imshow(z,cmap=plt.cm.Greys,interpolation='none',extent=[mini,maxi,minj,maxj],origin='lower left')
    cbar = fig.colorbar(cax1,orientation='vertical')
    ax1.set_title(title)
    plt.tight_layout()
    plt.show()
    plt.close()
    

def test0():
    """ """
    
    N = 5
    sigma_covij = 0.5
    p = (1.E-3,1.E-3)
    
    ii,jj = np.meshgrid(np.arange(N),np.arange(N),indexing='xy')
    rr = np.sqrt(ii**2.+jj**2.)
    covij = np.exp(-(rr**2./(2*sigma_covij**2.))) + 0.0005*sigma_covij
    
    
    i = ii.flatten().copy()
    j = jj.flatten().copy()
    
    fpred_N = np.zeros(len(i))
    fpred_E = np.zeros(len(i))
    fpred_S = np.zeros(len(i))
    fpred_W = np.zeros(len(i))
    
    for ix in range(len(i)):
        fpred_N[ix] = fpred_aijb(p,i[ix],j[ix],0)
        fpred_E[ix] = fpred_aijb(p,i[ix],j[ix],1)
        fpred_S[ix] = fpred_aijb(p,i[ix],j[ix],2)
        fpred_W[ix] = fpred_aijb(p,i[ix],j[ix],3)
        
    fpred_N = fpred_N.reshape(ii.shape)
    fpred_E = fpred_E.reshape(ii.shape)
    fpred_S = fpred_S.reshape(ii.shape)
    fpred_W = fpred_W.reshape(ii.shape)
    
    
    plot_maps_ftheta(ftheta_bound,ii,jj,r'$\theta_B$')
    plot_maps_ftheta(frdist,ii,jj,'f(r)')
    plot_map(np.log10(covij),ii,jj,title=r'$Cov_{ij}$')
    
    plot_map(fpred_N,ii,jj,title=r'$fpred_N$')
    plot_map(fpred_E,ii,jj,title=r'$fpred_E$')
    plot_map(fpred_S,ii,jj,title=r'$fpred_S$')
    plot_map(fpred_W,ii,jj,title=r'$fpred_W$')


def test_solve():
    """ """
    N=5
    sigma_covij = 0.5
    var = sigma_covij**2.
    mu = 1.
    
    ii,jj = np.meshgrid(np.arange(N),np.arange(N),indexing='xy')
    rr = np.sqrt(ii**2.+jj**2.)
    covij = sigma_covij**2. * mu * np.exp(-(rr**2./(2*(1.3)**2.))) #+ sigma_covij * 0.0005

    psmooth,epsmooth = solve_for_psmooth(covij,var,mu)
    
    Abest = solve_for_A_linalg(covij,var=1.,mu=1.)
    
    stop()

def test_selfconsist():
    """ """
    N=2
    var = 1.
    mu = 1.
    
    pin = [0.00237274,  0.13478362]
    
    #ii,jj = np.meshgrid(np.arange(N),np.arange(N),indexing='xy')
    
    covij = np.zeros((N,N,4),dtype='float32')
    
    for i in range(N):
        for j in range(N):
            covij[i,j,0] = fpred_aijb(pin,i,j,0)
            covij[i,j,1] = fpred_aijb(pin,i,j,1)
            covij[i,j,2] = fpred_aijb(pin,i,j,2)
            covij[i,j,3] = fpred_aijb(pin,i,j,3)
    
    covij = covij.sum(axis=2)
    
    #fig = plt.figure()
    #ax = fig.add_subplot(111)
    #ax.imshow(covij)
    #plt.show()
    
    Asynth = _build_aijx_synth(pin,N)
    #Abest = solve_for_A_linalg(covij,var=var,mu=mu,doplot=False)
    Abest = solve_for_A_linalg(covij,var=var,mu=mu,doplot=False,psmooth=[pin])
    
    singlepixmap = np.zeros((101,101),dtype='float32') + 0.01
    singlepixmap[50,50] = 1.
                
    kernel = degrade_estatic(singlepixmap,Abest)
    kernelsynth = degrade_estatic(singlepixmap,Asynth)
    
    fig = plt.figure()
    ax1 = fig.add_subplot(121)
    ax1.imshow(kernel)
    ax1.set_title('KERNEL')
    ax2 = fig.add_subplot(122)
    ax2.imshow(kernelsynth)
    ax2.set_title('KERNEL-SYNTH')
    plt.show()    
    
    
    fts.writeto('kernel_selfconsist.fits',kernel.transpose(),clobber=True)
    fts.writeto('kernel_selfconsist_synth.fits',kernelsynth.transpose(),clobber=True)
    
    stop()


    
if __name__ == '__main__':
    
    #test0()
    
    #test_solve()
    test_selfconsist()