#! /home/raf/SOFTWARE/anaconda2/envs/VISSIM/bin/python

import numpy as np
from scipy.optimize import curve_fit
from pdb import set_trace as stop
from matplotlib import pyplot as plt

def func(x, a, b, c):
    return a * np.exp(-b * x) + c

if __name__ == '__main__':

    xdata = np.linspace(0, 4, 50)
    y = func(xdata, 2.5, 1.3, 0.5)
    ydata = y + 0.2 * np.random.normal(size=len(xdata))

    popt, pcov = curve_fit(func, xdata, ydata)
    
    
    yfit = func(xdata,*popt)
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(xdata,ydata,'k-')
    ax.plot(xdata,yfit,'r--')
    plt.show()
    
    stop()
