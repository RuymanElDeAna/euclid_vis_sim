#! /unsafe/raf/SOFTWARE/anaconda/envs/VISSIM/bin/python
# -*- coding: utf-8 -*-
"""

This script generates the FITS and HK monitoring files for the previous day.
This is going to be called by a cronjob.

Created on Fri Sep 16 13:30:47 2016

@author: raf
"""

# IMPORT STUFF
from pdb import set_trace as stop
import os
from datetime import date
import sys
# END IMPORT

datapath = '/mssl3m/3/Projects/Euclid/vis/Char_Campaign/data'
scripts_path = '~/WORK/EUCLID/REPOS/VISSim/vissim/scripts'

if __name__ == '__main__':
    
    #os.system('ls /home/raf > /home/raf/test_17.txt')
    #sys.exit() # TEST
    
    cwd = os.path.abspath(os.path.curdir)
    
    today = date.today()
    yesterfolder = date.fromordinal(today.toordinal()-1).strftime('%d_%b_%y')
    #yesterfolder = '09_Sep_16' # TESTS
    
    pyesterfolder = os.path.join(datapath,yesterfolder) 

    print 'Looking for %s' % pyesterfolder
    #print cwd
    #os.system('which python >> /home/raf/whichpython')
    #sys.exit()
    
    if os.path.exists(pyesterfolder):
        os.chdir(datapath)
        
        #os.system('echo %s >> ~/tests_butler' % os.path.abspath(os.path.curdir))
        
        
        hkmonitor_comm = 'python %s/HKmonitor.py' % scripts_path
        
        hkmonitorline = '%s -p %s/ -r 1' % \
            (hkmonitor_comm,yesterfolder)
        
        os.system('which python >> ~/tests_butler')
        os.system('echo %s >> ~/tests_butler' % hkmonitorline)
        
        os.system(hkmonitorline)
        #sys.exit()
        
        fitsmonitor_comm = 'python %s/QLA.py' % scripts_path
        FITSmonitorline = '%s -c 2 -r 1 -p %s/' % \
            (fitsmonitor_comm,yesterfolder)
        os.system(FITSmonitorline)
        
        os.chdir(cwd)
        
        #stop()
        
        mailline = 'Monitor_Butler.py has just generated FITS and HK monitoring files\n'+\
        'for day %s\n\n' % yesterfolder +\
        'Check that out!'
        
        #os.system('echo "%s" | mail -s "Monitor Butler: %s" r.azzollini@ucl.ac.uk -c m.szafraniec@ucl.ac.uk' % \
        #   (mailline,yesterfolder))
    
