History vissim

    0.1: pre-development backbone.
    0.4: first version with most pieces together.
    0.5: this version has all the basic features present, but not fully tested.
    0.6: implemented pre/overscan, fixed a bug when an object was getting close to the upper right corner of an
         image it was not overlaid correctly. Included multiplicative flat fielding effect (pixel non-uniformity).
    0.7: implemented bleeding.
    0.8: cleaned up the code and improved documentation. Fixed a bug related to checking if object falls on the CCD.
         Improved the information that is being written to the FITS header.
    0.9: fixed a problem with the CTI model swapping Q1 with Q2. Fixed a bug that caused the pre- and overscan to
         be identical for each quadrant even though Q1 and 3 needs the regions to be mirrored.
    1.0: First release. The code can now take an over sampled PSF and use that for convolutions. Implemented a WCS
         to the header.
    1.05: included an option to add flux from the calibration unit to allow flat field exposures to be generated.
          Now scaled the number of cosmic rays with the exposure time so that 10s flats have an appropriate number
          of cosmic ray tracks.
    1.06: changed how stars are laid down on the CCD. Now the PSF is interpolated to a new coordinate grid in the
          oversampled frame after which it is downsampled to the CCD grid. This should increase the centroiding
          accuracy.
    1.07: included an option to apply non-linearity model. Cleaned the documentation.
    1.08: optimised some of the operations with numexpr (only a minor improvement).
    1.1: Fixed a bug related to adding the system readout noise. In previous versions the readout noise was
         being underestimated due to the fact that it was included as a variance not standard deviation.
    1.2: Included a spatially uniform scattered light. Changed how the image pixel values are rounded before
         deriving the Poisson noise. Included focal plane CCD gaps. Included a unittest.
    1.21: included an option to exclude cosmic background; separated dark current from background.
    1.25: changed to a bidirectional CDM03 model. This allows different CTI parameters to be used in parallel
          and serial directions.
    1.26: an option to include ghosts from the dichroic. The ghost model is simple and does not take into account
          the fact that the ghost depends on the focal plane position. Fixed an issue with image coordinates
          (zero indexing). Now input catalogue values agree with DS9 peak pixel locations.
    1.27: Convolution can now be performed using a GPU using CUDA if the hardware is available. Convolution mode
          is now controlled using a single parameter. Change from 'full' to 'same' as full provides no valid information
          over 'same'. In principle the 'valid' mode would give all valid information, but in practise it leads to
          truncated convolved galaxy images if the image and the kernel are of similar size.
    1.28: Moved the cosmic ray event generation to a separate class for easier management. Updated the code to
          generate more realistic looking cosmic rays. Included a charge diffusion smoothing to the cosmic rays
          to mimic the spreading of charge within the CCD. This is closer to reality, but probably still inaccurate
          given geometric arguments (charge diffusion kernels are measured using light coming from the backside of
          the CCD, while cosmic rays can come from any direction and penetrate to any depth).
    1.29: Fixed a bug in the object pixel coordinates for simulations other than the 0, 0 CCD. The FPA gaps
          were incorrectly taken into account (forcing the objects to be about 100 pixels of per gap).
    1.30: now nocti files contain ADC offset and readnoise, the same as the true output if CTI is simulated.self.information['mode']
    1.31: now a single FOLDER variable at the beginning of the program that should be set to
          point to the location of the vissim-python. Modified the ghost function, a fixed offset from the source, but
          more suitable for the correct input model.
    1.32: option to fix the random number generator seed using -f flag.
    1.4: included an option to simulate an image with the shutter open leading to readout trails.

    1.4.1 R. Azzollini effectively takes over.
          A few bugs in cosmicrays.py are corrected.
    1.4.2 prescanx set to 51
    1.4.3 Cosmic rays: substituted the CSF of lengths for the right one (instead of the distribution of Rayon Quadratique
          from Stardust). Also, changed default method to add cosmics as proportional to a flux, rather than add them 
          up to a coverage fraction. Coverage fraction is still an output in the log, but it is not commanded.
    1.4.4 Zodiacal light is added before applying PRNU, as it should.
    1.4.5 Scattered light is added before applying PRNU, as it should.
    1.4.6 dark current is "added" instead of "applied", and added before bleeding [negligible effect].
    1.4.7 dumped magzero keyword. Now using only zeropoint.
    1.4.8 added "cr_flux" keyword (cm-2 s-1), deprecated "coveringFactor"

    1.4.9 deprecated cosmic_bkgd, some changes in simulator.py to make it easier to run code from scripts
    1.5.0 Added new parameters in Charge Injection: ChInjOn, ChInjOff, ChInjRepeat. Now charge injection lines include
          non-uniformity. Horizontal charge injection lines have been deprecated.
    1.5.1 Added Trap Pumping Mode.
          Changed shutterOpen to ReadoutshutterOpen. This is to avoid confusion with IntegrationShutterOpen.
          Added IntegrationShutterOpen boolean to shut light during integration (all sources).
          changed "noise" keyword to "poisson".
    1.5.2 Added HKtools, QLAtools and latex.py to support, and HKmonitor.py and QLA.py to scripts.
    1.5.3 Added ds9.py to support, and quickds9.py to scripts. Debugging.
