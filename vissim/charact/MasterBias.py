# -*- coding: utf-8 -*-
"""

Flat-fielding Utilities.


Created on ... 28.Apr.2016, after lunch

@author: raf
"""

# IMPORT STUFF
from multiprocessing import Pool
from pdb import set_trace as stop
import numpy as np
import datetime
import warnings
from vissim.simulator.simulator import __version__

#from matplotlib import pyplot as plt

from vissim.datamodel import ccd as ccdmodule
from vissim.charact import FlatFielding as FF
#from vissim.support.files import cPickleDumpDictionary, cPickleRead
#from vissim.datamodel.HKtools import parseHKfiles

from scipy import ndimage as nd
#from scipy import signal
from astropy.io import fits as fts
# END IMPORT

Quads = ['E','F','G','H']


    
def produce_MasterBias(infits,outfits,savefits=True,settings={}):
    """Produces a Master Bias out of a number of BIAS exposures."""
    
    insettings = dict(cosmetics=None,bmethod='median',bscan='pre',
                      trimscan=[3,2],fit=True)
    insettings.update(settings)
    
    NAXIS1 = ccdmodule.NAXIS1/2
    NAXIS2 = ccdmodule.NAXIS2/2
    
    mbias = np.zeros((NAXIS1*2,NAXIS2*2))
        
    if insettings['cosmetics'] is not None:
        mask = insettings['cosmetics']
    else:
        mask = np.zeros((NAXIS1*2,NAXIS2*2))
    
    for Quad in Quads:
        
        B = ccdmodule.QuadBound[Quad]
        x0 = B[0]
        x1 = B[1]
        y0 = B[2]
        y1 = B[3]
    
        cubebias = np.zeros((NAXIS1,NAXIS2,len(infits)))
        
        Qmask = mask[x0:x1,y0:y1]
    
        for ix in range(len(infits)):
        
            print 'Loading img %i of %i..., Q=%s' % (ix+1,len(infits),Quad)
            iccdobj = ccdmodule.CCD(infits[ix])
            
            iccdobj.sub_offset(Quad,method=insettings['bmethod'],
                               scan=insettings['bscan'],trimscan=insettings['trimscan'])
            
            img = iccdobj.get_quad(Quad,canonical=False)
            
            img = np.ma.masked_array(img,Qmask)
            
            cubebias[:,:,ix] = img.copy()
        
        
        qbias = np.zeros((NAXIS1,NAXIS2))
        
        
        for ix in range(NAXIS1):
            qbias[ix,:] = np.nanmedian(cubebias[ix,:,:],axis=1)
        
        mbias[x0:x1,y0:y1] = qbias.copy()
    
    
    if insettings['fit']:
        
        pdegree = 5
        filtsize=15
        
        fbias = np.zeros_like(mbias,dtype='float32')
        
        for Quad in Quads:
            
             B = ccdmodule.QuadBound[Quad]
             
             data = mbias[B[0]:B[1],B[2]:B[3]]
             Qmask = mask[B[0]:B[1],B[2]:B[3]]
             
             mdata = np.ma.masked_array(data,Qmask)
             
             ILU = FF.get_ilum(mdata,pdegree=pdegree,filtsize=filtsize)
             
             fbias[B[0]:B[1],B[2]:B[3]] = ILU['polyfit']
    
    if savefits:
    
        hdu = fts.PrimaryHDU()
        hdu.header.add_history('Master BIAS')
        hdu.header.add_history('CCD273 EM1A Characterization Campaign')
        
        hdu.header['NCOMB'] = len(infits)
        
        hdu.header.add_history('Extension 1: AVERAGE')
        
        if insettings['fit']:
            hdu.header.add_history('Extension 2: 2DFIT')
        
        hdu.header.add_history('Created by VISsim (version=%s) at %s' % (__version__, datetime.datetime.isoformat(datetime.datetime.now())))
        hdu.header.add_history('Further Info: Ruyman Azzollini (r.azzollini_at_mssl.ucl.ac.uk)')
        
        hdulist = fts.HDUList([hdu])
        
        ahdu = fts.ImageHDU(mbias.transpose())
        ahdu.header['EXTNAME'] = 'AVERAGE'
        hdulist.append(ahdu)
        
        if insettings['fit']:
            fhdu = fts.ImageHDU(fbias.transpose())
            fhdu.header['EXTNAME'] = '2DFIT'
            fhdu.header['PDEGREE'] = pdegree
            fhdu.header['FILTSIZE'] = filtsize
            hdulist.append(fhdu)
        

        hdulist.writeto(outfits,clobber=True)
    
    return mbias
