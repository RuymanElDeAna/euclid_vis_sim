"""
CCD Spot Measurements
=====================

Azzollini:

Now I'll modify the Joint-Fit:

    - the positions are not fitted, but given as inputs (output from individual freq. fits).
    - pixel bounce is not forward modeled. Corrected upstream.
    - CCD psf changed to a pyramid model.
    - dependency of CCD widths with "peak" fluence is included in the model.
        - the input data must cover a range of fluences to get a meaningful fit.


Analyse laboratory CCD PSF measurements by forward modelling to the data.

The methods used here seem to work reasonably well when the spot has been well centred. If however, the
spot is e.g. 0.3 pixels off then estimating the amplitude of the Airy disc becomes rather difficult.
Unfortunately this affects the following CCD PSF estimates as well and can lead to models that are
rather far from the truth. Also, if when the width of the CCD PSF kernel becomes narrow, say 0.2, pixels
it is very difficult to recover. This most likely results from an inadequate sampling. In this case it might
be more appropriate to use "cross"-type kernel.

Because the amplitude can be very tricky to estimate, the version 1.4 (and later) implement a meta-parameter
called peak, which is the peak counts in the image. This is then converted to the amplitude by using the centroid
estimate. Because the centroids are fitted for each image, the amplitude can also vary in the joint fits. This
seems to improve the joint fitting constrains. Note however that this does couple the radius of the Airy disc
as well, because the amplitude estimate uses the x, y, and radius information as well.

One question to address is how the smoothing of the Airy disc is done. So far I have assumed that the Gaussian that
represents defocus should be centred at the same location as the Airy disc. However, if the displacement if the
Airy disc is large, then the defocus term will move the Airy disc to the direction of the displacement and make
it more asymmetric. Another option is to use scipy.ndimage.filters.gaussian_filter which simply applies Gaussian
smoothing to the input image. Based on the testing carried out this does not seem to make a huge difference. The
latter (smoothing) will lead to more or less the same CCD PSF estimates, albeit with slightly higher residuals.
We therefore adopt a Gaussian kernel that is centred with the Airy disc.

:requires: PyFITS
:requires: NumPy
:requires: SciPy
:requires: astropy
:requires: matplotlib
:requires: VISsim-Python
:requires: emcee
:requires: sklearn

:version: 1.9

:author: Sami-Matias Niemi & Ruyman Azzollini
:contact: r.azzollini@ucl.ac.uk
"""
import matplotlib
#matplotlib.use('pdf')
#matplotlib.rc('text', usetex=True)
matplotlib.rcParams['font.size'] = 17
matplotlib.rc('xtick', labelsize=14)
matplotlib.rc('axes', linewidth=1.1)
matplotlib.rcParams['legend.fontsize'] = 11
matplotlib.rcParams['legend.handlelength'] = 3
matplotlib.rcParams['xtick.major.size'] = 5
matplotlib.rcParams['ytick.major.size'] = 5
matplotlib.rcParams['image.interpolation'] = 'none'
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import pyfits as pf
import numpy as np
import emcee
import scipy
import scipy.ndimage.measurements as m
from scipy import signal
from scipy import ndimage
from scipy import optimize
from scipy.special import j1, jn_zeros
#from sklearn.gaussian_process import GaussianProcess
from astropy.modeling import models, fitting
import corner
import glob as g
import os, datetime
from multiprocessing import Pool
from pdb import set_trace as stop

from vissim.support import files as fileIO

__author__ = 'Sami-Matias Niemi, R. Azzollini'
__version__ = 0.1

#fixed parameters
cores = 8


def fitGauss2D(spot,xguess,yguess,xsigmaguess=0.5,ysigmaguess=0.5):
    """ """
    
    gauss = models.Gaussian2D(spot.max(), xguess, yguess, 
                              x_stddev=xsigmaguess, y_stddev=ysigmaguess) 
    
    gauss.theta.fixed = True  #fix angle
    p_init = gauss
    fit_p = fitting.LevMarLSQFitter()
    stopy, stopx = spot.shape
    X, Y = np.meshgrid(np.arange(0, stopx, 1), np.arange(0, stopy, 1))
    p = fit_p(p_init, X, Y, spot)
    #print p
    model = p(X, Y)
    
    return p, model
    





def forwardModelJointFit(inputimgs, idkey, wavelength, gain=3.1, size=10, burn=500, run=800,
                         spotx=[], spoty=[], simulated=False, truths=None,OBSIDS=[],cores=8):
    """
    Forward models the spot data found from the input files. Models all data simultaneously so that the Airy
    disc centroid and shift from file to file. Assumes that the spot intensity, focus, and the CCD PSF kernel
    are the same for each file. Can be used with simulated and real data.

    Note, however, that because the amplitude of the Airy disc is kept fixed, but the centroids can shift the
    former most likely forces the latter to be very similar. This is fine if we assume that the spots did not
    move during the time data were accumulated. So, this approach is unlikely to work if the projected spot
    drifted during the data acquisition.
    """
    print '\n\n\n'
    print '_'*120


    outputs = []

    nimages = len(inputimgs)
    orig = []
    images = []
    noise = []
    rns = []
    peakvalues = []
    xcentres = []
    ycentres = []
    
    for i,inputimg in enumerate(inputimgs):
        
        ispoty = spoty[i]
        ispotx = spotx[i]
        
        #get data and convert to electrons
        o = inputimg*gain

        if simulated:
            data = o
        else:
            #roughly the correct location - to avoid identifying e.g. cosmic rays
            data = o[ispoty-(size*3):ispoty+(size*3)+1, ispotx-(size*3):ispotx+(size*3)+1].copy()

        #maximum position within the cutout
        y, x = m.maximum_position(data)

        #spot and the peak pixel within the spot, this is also the CCD kernel position
        spot = data[y-size:y+size+1, x-size:x+size+1].copy()
        orig.append(spot.copy())
        
        #bias estimate
        if simulated:
            bias = 9000.
            rn = 4.5
        else:
            mask = np.zeros_like(o,dtype='bool')
            mask[ispoty-size:ispoty+size,ispotx-size:ispotx+size] = True
            
            bias = np.nanmedian(o[~mask])
            rn = np.std(o[~mask])
            
            #bias = np.median(o[spoty-size: spoty+size, spotx-220:spotx-20])
            #rn = np.std(o[spoty-size: spoty+size, spotx-220:spotx-20])
        
        print 'Readnoise (e):', rn
        if rn < 2. or rn > 6.:
            print 'NOTE: suspicious readout noise estimate...'
        print 'ADC offset (e):', bias

        #remove bias
        spot -= bias

        #set highly negative values to zero
        spot[spot + rn**2 < 0.] = 0.

        print 'Least Squares Fitting...'
        
        p, model = fitGauss2D(spot,size,size,xsigmaguess=0.5,ysigmaguess=0.5)
        
        max = np.max(spot)
        s = spot.sum()
        print 'Maximum Value:', max
        print 'Sum:', s
        print ''

        peakvalues.append(max)

        #noise model
        variance = spot.copy() + rn**2

        #save to a list
        images.append(spot)
        noise.append(variance)
        xcentres.append(p.x_mean.value)
        ycentres.append(p.y_mean.value)
        rns.append(rn**2)


    peakvalues = np.asarray(peakvalues)
    peakrange_all = (np.min(peakvalues), np.max(peakvalues))

    print 'Peak Range:', peakrange_all
    
    peakranges = []
    for ip,p in enumerate(peakvalues):
        peakranges.append((0.5*p,2.*p))

    #MCMC based fitting
    ndim = nimages + 6  # amplitude for each image and single radius, focus, and (CCD_m (slope) and CCD_b (intercept) ) x 2
    nwalkers = 1000
    print '\n\nBayesian Fitting, model has %i dimensions' % ndim

    # Choose an initial set of positions for the walkers using the Gaussian fit
    tmp = _expectedValues()[wavelength]
    print 'Using initial guess [radius, focus, width_x, width_y]:', tmp
    p0 = np.zeros((nwalkers, ndim))
    for x in xrange(nimages):
        p0[:, x] = np.random.normal(peakvalues[x], peakvalues[x]/100, size=nwalkers)              # amplitudes
    p0[:, -6] = np.random.normal(tmp[0], 0.01, size=nwalkers)                   # radius
    p0[:, -5] = np.random.normal(tmp[1], 0.01, size=nwalkers)                   # focus
    p0[:, -4] = np.random.normal(0.,0.01,size=nwalkers)                         # width_x slope
    p0[:, -3] = np.random.normal(tmp[2], 0.01, size=nwalkers)                   # width_x intercept
    p0[:, -2] = np.random.normal(0.,0.01,size=nwalkers)                         # width_y slope
    p0[:, -1] = np.random.normal(tmp[3], 0.01, size=nwalkers)                   # width_y intercept

    # Initialize the sampler with the chosen specs.
    #Create the coordinates x and y
    x = np.arange(0, spot.shape[1])
    y = np.arange(0, spot.shape[0])
    #Put the coordinates in a mesh
    xx, yy = np.meshgrid(x, y)

    #Flatten the arrays
    xx = xx.flatten()
    yy = yy.flatten()

    #initiate sampler

    if cores != 0:

        pool = Pool(cores) #A hack Dan gave me to not have ghost processes running as with threads keyword
        sampler = emcee.EnsembleSampler(nwalkers, ndim, log_posteriorJoint_CCDline,
                                    args=[xx, yy, xcentres, ycentres, images, rns, peakranges, spot.shape], pool=pool)
    else:
        sampler = emcee.EnsembleSampler(nwalkers, ndim, log_posteriorJoint_CCDline,
                                    args=[xx, yy, xcentres, ycentres, images, rns, peakranges, spot.shape])
                                    
    # Run a burn-in and set new starting position
    print "Burning-in..."
    pos, prob, state = sampler.run_mcmc(p0, burn)
    best_pos = sampler.flatchain[sampler.flatlnprobability.argmax()]
    print best_pos
    print "Mean acceptance fraction:", np.mean(sampler.acceptance_fraction)
    pos = emcee.utils.sample_ball(best_pos, best_pos/100., size=nwalkers)
    # Reset the chain to remove the burn-in samples.
    sampler.reset()

    #run another burn-in
    print "Running an improved estimate..."
    pos, prob, state = sampler.run_mcmc(pos, burn)
    print "Mean acceptance fraction:", np.mean(sampler.acceptance_fraction)
    sampler.reset()

    # Starting from the final position in the improved chain
    print "Running final MCMC..."
    pos, prob, state = sampler.run_mcmc(pos, run, rstate0=state)
    print "Mean acceptance fraction:", np.mean(sampler.acceptance_fraction)

    #Get the index with the highest probability
    maxprob_index = np.argmax(prob)

    #Get the best parameters and their respective errors and print best fits
    params_fit = pos[maxprob_index]
    errors_fit = [sampler.flatchain[:,i].std() for i in xrange(ndim)]
    print params_fit

    #unpack the fixed parameters
    radius, focus, wx_sl, wx_ic, wy_sl, wy_ic = params_fit[-6:]
    radiusE, focusE, wx_slE, wx_icE, wy_slE, wy_icE = errors_fit[-6:]

    #print results
    
    _printFWHM(wx_sl, wy_sl, wx_slE, wy_slE)
    _printFWHM(wx_ic, wy_ic, wx_icE, wy_icE)
    

    #save the best models per file
    size = size*2 + 1
    gofs = []
    mdiff = []
    for index, ignore in enumerate(inputimgs):
        #path, file = os.path.split(file)
        root = '%s_%s' % (idkey, index)
        #X and Y are always in pairs
        center_x = xcentres[index]
        center_y = ycentres[index]
        
        peak = params_fit[index]

        #1)Generate a model Airy disc
        amplitude = _amplitudeFromPeak(peak, center_x, center_y, radius,
                                       x_0=int(size/2.-0.5), y_0=int(size/2.-0.5))
        airy = models.AiryDisk2D(amplitude, center_x, center_y, radius)
        adata = airy.evaluate(xx, yy, amplitude, center_x, center_y, radius).reshape((size, size))

        #2)Apply Focus
        f = models.Gaussian2D(1., center_x, center_y, focus, focus, 0.)
        focusdata = f.evaluate(xx, yy, 1., center_x, center_y, focus, focus, 0.).reshape((size, size))
        model = signal.convolve2d(adata, focusdata, mode='same')

        #3)Apply CCD diffusion, approximated with a Gaussian
        
        wx,wy = getwidths(peak,(wx_sl,wx_ic),(wy_sl,wy_ic))

        CCDdata = np.array([[0.0, wy, 0.0],
                        [wx, (1.-wy-wy-wx-wx), wx],
                        [0.0, wy, 0.0]])
        
        model = signal.convolve2d(model, CCDdata, mode='same') #.flatten()

        #save the data, model and residuals
        fileIO.writeFITS(orig[index], root+'_data.fits', int=False)
        outputs.append(root+'_data.fits')
        fileIO.writeFITS(images[index], root+'_datafit.fits', int=False)
        outputs.append(root+'_datafit.fits')
        fileIO.writeFITS(model, root+'_model.fits', int=False)
        outputs.append(root+'_model.fits')
        fileIO.writeFITS(model - images[index], root+'_residual.fits', int=False)
        outputs.append(root+'_residual.fits')
        fileIO.writeFITS(((model - images[index])**2 / noise[index]), root+'_residualSQ.fits', int=False)
        outputs.append(root+'_residualSQ.fits')

        #a simple goodness of fit
        gof = (1./(np.size(images[index])*nimages - ndim)) * np.sum((model - images[index])**2 / noise[index])
        maxdiff = np.max(np.abs(model - images[index]))
        print 'GoF:', gof, ' Max difference', maxdiff
        gofs.append(gof)
        mdiff.append(maxdiff)
        print 'Amplitude Estimate:', amplitude

    if np.asarray(mdiff).max() > 3e3 or np.asarray(gofs).max() > 4.:
        print '\nFIT UNLIKELY TO BE GOOD...\n'

    #save results
    res = dict(wx_sl=wx_sl, wx_ic=wx_ic, wy_sl=wy_sl, wy_ic=wy_ic, 
               wx_slE=wx_slE, wx_icE=wx_icE, wy_slE=wy_slE,wy_icE=wy_icE,
               idkey=idkey,wavelength=wavelength, peakvalues=np.asarray(peakvalues), 
               CCDmodeldata=CCDdata,
               GoFs=gofs, fit=params_fit, maxdiff=mdiff,OBSIDS=OBSIDS,
               fittype='Joint_noPB_CCDkernelslope')
    fileIO.cPickleDumpDictionary(res, root + '.pkl')
    outputs.append(root+'.pkl')

    #plot
    samples = sampler.chain.reshape((-1, ndim))
    #extents = None
    #if simulated:
    #    extents = [(0.9*truth, 1.1*truth) for truth in truths]
    #    print extents

    fig = corner.corner(samples, labels=['peak']*nimages + ['radius', 'focus', 'wx_sl', 'wx_ic', 'wy_sl', 'wy_ic'],
                          truths=truths)#, extents=extents)
    fig.savefig(idkey+'_Triangle.png')
    plt.close()
    pool.close()
    
    outputs.append(idkey+'_Triangle.png')
    
    return outputs


def getwidths(peak,wxp,wyp):
    wx = wxp[1] + wxp[0] * peak/2.E5
    wy = wyp[1] + wyp[0] * peak/2.E5        
    return wx,wy

#[xx, yy, xcentres, ycentres, images, rns, peakranges, spot.shape]
def log_posteriorJoint_CCDline(theta, x, y, xc, yc, z, var, peakranges, size):
    """
    Posterior probability: combines the prior and likelihood.
    """
    lp = log_priorJoint(theta, peakranges)

    if not np.isfinite(lp):
        return -np.inf

    return lp + log_likelihoodJoint(theta, x, y, xc, yc, z, var, size)


def log_priorJoint(theta, peakranges):
    """
    Priors, limit the values to a range but otherwise flat.
    """
    #[amplitude]*images + [radius, focus, wx_sl, wx_ic, wy_sl, wy_yc])
    
    peakdisc = np.all([peakranges[i][0] < theta[i] < peakranges[i][1] for i in range(len(peakranges))])
    
    if peakdisc and 0.1 < theta[-6] < 0.8 and \
       0.1 < theta[-5] < 0.8 and (-1.5< theta[-4] < 1.5) and (0.1 < theta[-3] < 0.4) and \
       (-1.5< theta[-2] < 1.5) and (0.1 < theta[-1] < 0.4):
        return 0.
    else:
        return -np.inf


# theta, x, y, xc, yc, z, var, size
def log_likelihoodJoint(theta, x, y, xc, yc, data, var, size):
    """
    Logarithm of the likelihood function for joint fitting. Not really sure if this is right...
    """
    #unpack the parameters
    #[xpos, ypos]*images) +[amplitude, radius, focus])
    nimages = len(theta[:-6])
    radius, focus, wx_sl, wx_ic, wy_sl, wy_ic = theta[-6:]

    lnL = 0.
    for tmp in xrange(nimages):
        
        center_x = xc[tmp]
        center_y = yc[tmp]
        
        peak = theta[tmp]

        #1)Generate a model Airy disc
        amplitude = _amplitudeFromPeak(peak, center_x, center_y, radius,
                                       x_0=int(size[0]/2.-0.5), y_0=int(size[1]/2.-0.5))
        airy = models.AiryDisk2D(amplitude, center_x, center_y, radius)
        adata = airy.evaluate(x, y, amplitude, center_x, center_y, radius).reshape(size)

        #2)Apply Focus, no normalisation as smoothing
        f = models.Gaussian2D(1., center_x, center_y, focus, focus, 0.)
        focusdata = f.evaluate(x, y, 1., center_x, center_y, focus, focus, 0.).reshape(size)
        model = signal.convolve2d(adata, focusdata, mode='same')

        #3)Apply CCD diffusion (peak-value dependent! bc. of BF)

        wx,wy = getwidths(peak,(wx_sl,wx_ic),(wy_sl,wy_ic))

        CCDdata = np.array([[0.0, wy, 0.0],
                        [wx, (1.-wy-wy-wx-wx), wx],
                        [0.0, wy, 0.0]])
        
        model = signal.convolve2d(model, CCDdata, mode='same').flatten()

        #lnL += - 0.5 * np.sum((data[tmp].flatten() - model)**2 / var[tmp].flatten())
        #Gary B. said that this should be from the model not data so recompute var (now contains rn**2)
        var = var[tmp] + model.copy()
        lnL += - 0.5 * np.sum((data[tmp].flatten() - model)**2 / var)

    return lnL


def _printResults(best_params, errors):
    """
    Print basic results.
    """
    print("=" * 60)
    print('Fitting with MCMC:')
    pars = ['peak', 'center_x', 'center_y', 'radius', 'focus', 'width_x', 'width_y']
    print('*'*20 + ' Fitted parameters ' + '*'*20)
    for name, value, sig in zip(pars, best_params, errors):
        print("{:s} = {:e} +- {:e}" .format(name, value, sig))
    print("=" * 60)


def _simulate(theta=(1.e5, 10., 10.3, 0.6, 0.1, 10., 10., 0.33, 0.35), gain=3.1, bias=9000, rn=4.5, size=21,
              out='simulated.fits', Gaussian=True):
    """
    Generate simulated spot image with the assumed process.
    """
    #unpack the parameters
    amplitude, center_x, center_y, radius, focus, xCCD, yCCD, width_x, width_y = theta

    #Create the coordinates x and y
    x = np.arange(0, size)
    y = np.arange(0, size)
    #Put the coordinates in a mesh
    x, y = np.meshgrid(x, y)

    #1)Generate a model Airy disc
    airy = models.AiryDisk2D(amplitude, center_x, center_y, radius)
    adata = airy.evaluate(x, y, amplitude, center_x, center_y, radius).reshape((size, size))

    #2)Apply Focus
    #data = ndimage.filters.gaussian_filter(adata, [width_y, width_x]) #no position
    f = models.Gaussian2D(1., center_x, center_y, focus, focus, 0.)
    focusdata = f.evaluate(x, y, 1., center_x, center_y, focus, focus, 0.).reshape((size, size))
    data = signal.convolve2d(adata, focusdata, mode='same')

    #3)Apply CCD diffusion
    if Gaussian:
        #full Gaussian
        CCD = models.Gaussian2D(1., xCCD, yCCD, width_x, width_y, 0.)
        d = CCD.evaluate(x, y, 1.,xCCD, yCCD, width_x, width_y, 0.).reshape((size, size)) #max = 1 as centred
        CCDdata = signal.convolve2d(data, d, mode='same')
    else:
        #CCD kernel -- high flux
        kernel = np.array([[0.01/4., 0.05, 0.01/4.],
                          [0.075, 0.74, 0.075],
                          [0.01/4., 0.05, 0.01/4.]])
        fileIO.writeFITS(kernel, 'kernel.fits', int=False)
        CCDdata = ndimage.convolve(data, kernel)

    #4)Add Poisson noise
    rounded = np.rint(CCDdata)
    residual = CCDdata.copy() - rounded #ugly workaround for multiple rounding operations...
    rounded[rounded < 0.0] = 0.0
    CCDdata = np.random.poisson(rounded).astype(np.float64)
    CCDdata += residual

    #5)Add ADC offset level
    CCDdata += bias

    #6)Add readnoise
    CCDdata += np.random.normal(loc=0.0, scale=rn, size=CCDdata.shape)

    #7)Convert to DNs
    CCDdata = np.round(CCDdata/gain)

    #save to a file
    fileIO.writeFITS(CCDdata, out)


def _printFWHM(sigma_x, sigma_y, sigma_xerr, sigma_yerr, req=10.8):
    """
    Print results and compare to the requirement at 800nm.
    """
    print("=" * 60)
    print 'FWHM (requirement %.1f microns):' % req
    print round(np.sqrt(_FWHMGauss(sigma_x)*_FWHMGauss(sigma_y)), 2), ' +/- ', \
          round(np.sqrt(_FWHMGauss(sigma_xerr)*_FWHMGauss(sigma_yerr)), 3) , ' microns'
    print 'x:', round(_FWHMGauss(sigma_x), 2), ' +/- ', round(_FWHMGauss(sigma_xerr), 3), ' microns'
    print 'y:', round(_FWHMGauss(sigma_y), 2), ' +/- ', round(_FWHMGauss(sigma_yerr), 3), ' microns'
    print("=" * 60)


def _FWHMGauss(sigma, pixel=12):
    """
    Returns the FWHM of a Gaussian with a given sigma.
    The returned values is in microns (pixel = 12microns).
    """
    return sigma*2*np.sqrt(2*np.log(2))*pixel


def _ellipticityFromGaussian(sigmax, sigmay):
    """
    Ellipticity
    """
    return np.abs((sigmax**2 - sigmay**2) / (sigmax**2 + sigmay**2))


def _ellipticityerr(sigmax, sigmay, sigmaxerr, sigmayerr):
    """
    Error on ellipticity.
    """
    e = _ellipticityFromGaussian(sigmax, sigmay)
    err = e * np.sqrt((sigmaxerr/e)**2 + (sigmayerr/e)**2)
    return err


def _R2FromGaussian(sigmax, sigmay, pixel=0.1):
    """
    R2.
    """
    return (sigmax*pixel)**2 + (sigmay*pixel)**2


def _R2err(sigmax, sigmay, sigmaxerr ,sigmayerr):
    """
    Error on R2.
    """
    err = np.sqrt((2*_R2FromGaussian(sigmax, sigmay))**2*sigmaxerr**2 +
                  (2*_R2FromGaussian(sigmax, sigmay))**2*sigmayerr**2)
    return err


def RunTestSimulations(newfiles=True):
    """
    A set of simulated spots and analysis.

    Note that this is not the best test case for the joint fitting, because the amplitudes are fixed.
    This means that the peak pixels can have quite different values. With the ones selected here,
    the fourth is actually somewhat different from the others. It is actually quite nice that we
    can still recover the CCD PSF.
    """
    print("|" * 120)
    print 'SIMULATED DATA'
    #a joint fit test - vary only the x and y positions
    #It is misleading to keep the amplitude fixed as it is the counts in the peak pixel that matters.
    #If the Airy were centred perfectly then we could keep the amplitude fixed. In this case the individual
    #fits will work and can recover the 200k amplitude, but it is more problematic for the joint fit.
    theta1 = (2.e5, 9.9, 10.03, 0.47, 0.41, 10., 10., 0.291, 0.335)
    theta2 = (2.e5, 10.1, 9.97, 0.47, 0.41, 10., 10., 0.291, 0.335)
    theta3 = (2.e5, 9.97, 10.1, 0.47, 0.41, 10., 10., 0.291, 0.335)
    theta4 = (2.e5, 10.02, 9.9, 0.47, 0.41, 10., 10., 0.291, 0.335)
    theta5 = (2.e5, 10.1, 10., 0.47, 0.41, 10., 10., 0.291, 0.335)

    thetas = [theta1, theta2, theta3, theta4, theta5]

    for i, theta in enumerate(thetas):
        if newfiles:
            print 'Generating a new file with the following parameters:'
            _simulate(theta=theta, out='simulated/simulatedJoint%i.fits' %i)

            print 'amplitude, x, y, radius, focus, width_x, width_y'
            print theta[0], theta[1], theta[2], theta[3], theta[4], theta[7], theta[8]
            print("=" * 60)

        forwardModel(file='simulated/simulatedJoint%i.fits' %i, out='simulatedResults/RunI%i' %i, simulation=True,
                     truths=[theta[0], theta[1], theta[2], theta[3], theta[4], theta[7], theta[8]])

    #plot residuals
    _plotModelResiduals(id='RunI0', folder='simulatedResults/', out='Residual0.pdf', individual=True)
    _plotModelResiduals(id='RunI1', folder='simulatedResults/', out='Residual1.pdf', individual=True)
    _plotModelResiduals(id='RunI2', folder='simulatedResults/', out='Residual2.pdf', individual=True)
    _plotModelResiduals(id='RunI3', folder='simulatedResults/', out='Residual3.pdf', individual=True)
    _plotModelResiduals(id='RunI4', folder='simulatedResults/', out='Residual4.pdf', individual=True)

    #joint fit
    truths = [theta1[1], theta1[2], theta2[1], theta2[2], theta3[1], theta3[2], theta4[1], theta4[2],
              theta5[1], theta5[2], theta1[0], theta4[3], theta1[4], theta1[7], theta1[8]]
    forwardModelJointFit(g.glob('simulated/simulatedJoint?.fits'),
                         out='simulated800nmJoint', wavelength='800nm', simulated=True,
                         truths=truths)

    print 'True width_x and widht_y:', theta1[7], theta1[8]

    #plot residuals
    _plotModelResiduals(id='simulated800nmJoint0', folder='results/', out='ResidualJ0.pdf')
    _plotModelResiduals(id='simulated800nmJoint1', folder='results/', out='ResidualJ1.pdf')
    _plotModelResiduals(id='simulated800nmJoint2', folder='results/', out='ResidualJ2.pdf')
    _plotModelResiduals(id='simulated800nmJoint3', folder='results/', out='ResidualJ3.pdf')
    _plotModelResiduals(id='simulated800nmJoint4', folder='results/', out='ResidualJ4.pdf')

    #test plots
    _plotDifferenceIndividualVsJoined(individuals='simulatedResults/RunI*.pkl',
                                      joined='results/simulated800nmJoint.pkl',
                                      title='Simulated Data', truthx=theta1[7], truthy=theta1[8],
                                      requirementE=None, requirementFWHM=None, requirementR2=None)


def RunTestSimulations2(newfiles=True):
    #different simulation sets
    print("|" * 120)
    print 'Single Fitting Simulations'
    theta1 = (1.e6, 9.65, 10.3, 0.6, 0.45, 10., 10., 0.28, 0.33)    # ok recovery, sigmax has long tail towards 0.
    theta2 = (5.e5, 10.3, 10.2, 0.55, 0.45, 10., 10., 0.38, 0.36)   # well recovered
    theta3 = (8.e4, 10.0, 10.1, 0.4, 0.55, 10., 10., 0.25, 0.35)    # ok, recovery, but sigmax has long tail towards 0.
    theta4 = (5.e5, 10.1, 10.3, 0.42, 0.48, 10., 10., 0.30, 0.28)   # sigmax and sigmay not perfectly recovered
    theta5 = (2.e5, 9.95, 10.3, 0.45, 0.5, 10., 10., 0.33, 0.35)    # good recovery
    thetas = [theta1, theta2, theta3, theta4, theta5]

    for i, theta in enumerate(thetas):
        if newfiles:
            _simulate(theta=theta, out='simulated/simulatedSmall%i.fits' %i)
        forwardModel(file='simulated/simulatedSmall%i.fits' %i, out='simulatedResults/Run%i' %i, simulation=True,
                     truths=[theta[0], theta[1], theta[2], theta[3], theta[4], theta[7], theta[8]])
        print("=" * 60)
        print 'Simulation Parameters'
        print 'amplitude, center_x, center_y, radius, focus, width_x, width_y'
        print theta[0], theta[1], theta[2], theta[3], theta[4], theta[7], theta[8]
        print("=" * 60)


def _amplitudeFromPeak(peak, x, y, radius, x_0=10, y_0=10):
    """
    This function can be used to estimate an Airy disc amplitude from the peak pixel, centroid and radius.
    """
    rz = jn_zeros(1, 1)[0] / np.pi
    r = np.sqrt((x - x_0) ** 2 + (y - y_0) ** 2) / (radius / rz)
    if r == 0.:
        return peak
    rt = np.pi * r
    z = (2.0 * j1(rt) / rt)**2
    amp = peak / z
    return amp


def _peakFromTruth(theta, size=21):
    """
    Derive the peak value from the parameters used for simulations.
    """
    amplitude, center_x, center_y, radius, focus, width_x, width_y = theta
    x = np.arange(0, size)
    y = np.arange(0, size)
    x, y = np.meshgrid(x, y)
    airy = models.AiryDisk2D(amplitude, center_x, center_y, radius)
    adata = airy.evaluate(x, y, amplitude, center_x, center_y, radius)
    return adata.max()


def _expectedValues():
    """
    These values are expected for well exposed spot data. The dictionary has a tuple for each wavelength.
    Note that for example focus is data set dependent and should be used only as an indicator of a possible value.

    keys: l600, l700, l800, l890

    tuple = [radius, focus, widthx, widthy]
    """
    out = dict(l600=(0.45, 0.40, 0.34, 0.32),
               l700=(0.47, 0.40, 0.32, 0.31),
               l800=(0.49, 0.41, 0.30, 0.30),
               l800l=(0.49, 0.41, 0.27, 0.27),
               l800m=(0.49, 0.41, 0.30, 0.30),
               l800h=(0.49, 0.41, 0.31, 0.31),
               l890=(0.54, 0.38, 0.29, 0.29))

    return out

