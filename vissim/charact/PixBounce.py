#! /unsafe/raf/SOFTWARE/anaconda/envs/VISSIM/bin/python
# -*- coding: utf-8 -*-
"""

Functions related to PIXEL BOUNCE effect.

Created on Thu Aug 18 13:18:43 2016

@author: raf
"""

# IMPORT STUFF
import numpy as np
from pdb import set_trace as stop

import matplotlib
matplotlib.rcParams['font.size'] = 17
matplotlib.rc('xtick', labelsize=14)
matplotlib.rc('axes', linewidth=1.1)
matplotlib.rcParams['legend.fontsize'] = 12
matplotlib.rcParams['legend.handlelength'] = 3
matplotlib.rcParams['xtick.major.size'] = 5
matplotlib.rcParams['ytick.major.size'] = 5
matplotlib.rcParams['image.interpolation'] = 'none'
from matplotlib import pyplot as plt

# END IMPORT

sag_profile = np.array([ -8.14599392e-04,-3.26418871e-04,-1.25027698e-04,
        -4.64350633e-05,-1.48303789e-05,-3.42100884e-06,
        -1.88329068e-07], dtype='float32')

sag_coeffs = [-8.15e-4,1.085]


def sag_func(dpix,ea,ew):
    """ """
    return ea*np.exp(-dpix/ew)


def sag_image(img,sag_coeffs=sag_coeffs,rnode='left'):
    """Adds pixel-sag effect to an image.
    
    :param img: float, image
    :param sag_coeffs: list, sag-effect coefficients
    :param rnode: char, readout node is to the "left" or to the "right".
    
    """
    
    NY,NX = img.shape
    
    nsag = 7
    pb_a,pb_w = sag_coeffs
    dpix = np.arange(nsag)
    
    sag_profile = sag_func(dpix,pb_a,pb_w)
    sag_profile = np.repeat(sag_profile.reshape(1,nsag),NY,axis=0)
    
    simg = img.copy()
    
    if rnode == 'left':
    
        
        for ix in range(NX-1):
            
            ixend = min(ix+1+nsag,NX)
            sublen = ixend - (ix+1) #+ 1
            
            simg[:,ix+1:ixend] += (sag_profile[:,0:sublen] * simg[:,ix][:,np.newaxis])
    
    elif rnode == 'right':
    
        for ix in range(NX-1,0,-1):
            
            ixend = max(ix-nsag,0)
            sublen = ix-1 - ixend + 1
            simg[:,ixend:ix] += (sag_profile[:,0:sublen] * simg[:,ix][:,np.newaxis])[:,::-1]
    
    
    return simg

def desag_image(img,sag_coeffs=sag_coeffs,rnode='left'):
    """Corrects pixel-sag effect on an image.
    
    :param img: float, image
    :param sag_coeffs: list, sag-effect coefficients
    :param rnode: char, readout node is to the "left" or to the "right"."""
    
    NY,NX = img.shape
    
    nsag = 7
    pb_a,pb_w = sag_coeffs
    dpix = np.arange(nsag)
    
    sag_profile = sag_func(dpix,pb_a,pb_w)
    sag_profile = np.repeat(sag_profile.reshape(1,nsag),NY,axis=0)     
    
    
    dimg = img.copy()
    
    if rnode == 'left':
    
        
        for ix in range(NX-1):
            
            ixend = min(ix+1+nsag,NX)
            sublen = ixend - (ix+1) #+ 1
            
            dimg[:,ix+1:ixend] -= (sag_profile[:,0:sublen] * img[:,ix][:,np.newaxis])
    
    elif rnode == 'right':
    
        for ix in range(NX-1,0,-1):
            
            ixend = max(ix-nsag,0)
            sublen = ix-1 - ixend + 1
            dimg[:,ixend:ix] -= (sag_profile[:,0:sublen] * img[:,ix][:,np.newaxis])[:,::-1]
    
    
    return dimg

    
def test():
    """ """
    
    img = np.ones((2,100)) * 100.
    img[:,0:41] = 10.
    img[:,60:] = 10.
    
    sag_coeffs = [-0.1,1.]
    
    sagged_left = sag_image(img,sag_coeffs=sag_coeffs,rnode='left')
    sagged_right = sag_image(img,sag_coeffs=sag_coeffs,rnode='right')
    
    img_1d = img.mean(axis=0)
    sagged_left_1d = sagged_left.mean(axis=0)
    sagged_right_1d = sagged_right.mean(axis=0)
    
    desagged_left = desag_image(sagged_left,sag_coeffs=sag_coeffs,rnode='left')
    desagged_right = desag_image(sagged_right,sag_coeffs=sag_coeffs,rnode='right')
    
    desagged_left_1d = desagged_left.mean(axis=0)
    desagged_right_1d = desagged_right.mean(axis=0)
    
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(img_1d,label='original')
    ax.plot(sagged_left_1d,label='sagged left')
    ax.plot(sagged_right_1d,label='sagged right')
    ax.set_xlabel('pixel')
    ax.set_ylabel('a.u.')
    ax.set_ylim([-20,120])
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles,labels,loc='lower right')
    ax.set_title('PixBounce Test: Sagged')
    
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(img_1d,label='original')
    ax.plot(desagged_left_1d,'o-',label='desagged left')
    ax.plot(desagged_right_1d,'s-',label='desagged right')
    ax.set_xlabel('pixel')
    ax.set_ylabel('a.u.')
    #ax.set_xlim([35,65])
    ax.set_ylim([-20,120])
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles,labels,loc='lower right')
    ax.set_title('PixBounce Test: Desagged')
    
    plt.show()
    
    
    stop()
    
if __name__ == '__main__': 
    test()